###to define a varaible. 
variable "aws_region" {
  type    = string #number #map #listofstring
  default = "us-east-1"
}

variable "instance_type" {
  type    = string
  default = "t3.micro"
}
variable "instance_keypair" {
  type    = string
  default = "terraform-key"
}
variable "business_devision" {
  type    = string
  default = "sap"
}

variable "environment" {
  type    = string
  default = "dev"
}
variable "sandboxes" {
  type = map(object({
    instance_type = string,
    tags          = map(string)
  }))
  #each.value.instance_type
  #each.value.tags
  #type = string
  #default = sandbox_one
  default = {
    sandbox_one = {
      instance_type = "t2.small"
      tags = {
        Name = "sandbox_one"
      }
    },
    sandbox_two = {
      instance_type = "t2.micro"
      tags = {
        Name = "sandbox_two"
      }
    },
    sandbox_three = {
      instance_type = "t2.nano"
      tags = {
        Name = "sandbox_three"
      }
    }
  }
}