
resource "aws_instance" "myec2vm" {
  for_each = var.sandboxes
  ami                    = data.aws_ami.amzlinux2.id
  instance_type          = each.value["instance_type"]
  key_name               = var.instance_keypair
  vpc_security_group_ids = [aws_security_group.allow_ssh.id, aws_security_group.allow_http.id]
  user_data              = file("${path.module}/app-script/app-script.sh")
  #count = 2
  #for_each = toset(var.sandboxes)
  #toset it takes a list type and converts to a set. set is unorder collection of unqiue value
  #for i=0 i++ i>tosetvalue
  tags = each.value["tags"]
  ##every resource will be identified using a index number
  #instead of a value 
}
